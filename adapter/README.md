# Simple Adapter

## The adapter patter is one of the simplest patterns in which an interface is used to connect two otherwise incompatible types.

### There are two examples of the adapter pattern shown here. The first is a simple printer example. The second is joining two incompatible interfaces into one interface.
