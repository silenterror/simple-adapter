// DotMatrix - this is the legacy printer class
class DotMatrix {
    constructor() {
        this.print = function (s) {
            const message = `Dot Matrix: ${s}`;
            return message;
        };
    }
}

// LaserAdapter - this is the adapter class that adapts
// the legacy printer class to the new interface
class LaserAdapter {
    constructor(legacy, message) {
        this.print_message = function () {
            if (legacy) {
                return legacy.print(`Adapter: ${message}`);
            }
            return message;
        };
    }
}

// create a legacy printer
legacy_printer = new DotMatrix();

// create an adapter for the legacy printer
adapter = new LaserAdapter(legacy_printer, "Legacy Test Print");
console.log(adapter.print_message());

// set the legacy to null to fall through to the laser printer
adapter = new LaserAdapter(null, "Laser Test Print");
console.log(adapter.print_message());
