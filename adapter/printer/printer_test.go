package printer

import "testing"

func TestLegacyPrinter(t *testing.T) {
	// create a new instance of the legacy printer.
	legacyPrinter := &DotMatrix{}

	// create a new instance of the adapter and pass in the legacy printer.
	adapter := &LaserAdapter{
		legacy:  legacyPrinter,
		message: "Legacy Printer Test Print",
	}

	// print the message using the adapter or modern printer.
	t.Log(adapter.PrintMessage())
}

func TestLaserPrinter(t *testing.T) {
	// create a new instance of the adapter and pass in the legacy printer.
	adapter := &LaserAdapter{
		legacy:  nil,
		message: "Laser Printer Test Print",
	}

	// print the message using the adapter or modern printer.
	t.Log(adapter.PrintMessage())
}
