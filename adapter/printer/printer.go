package printer

import (
	"fmt"
	"log"
)

// LegacyPrinter - this is the interface that the legacy printer will implement.
type LegacyPrinter interface {
	Print(s string) string
}

// DotMatrix - this is the struct that implements the LegacyPrinter interface.
type DotMatrix struct{}

// Print - this is the method that implements the LegacyPrinter interface.
func (l *DotMatrix) Print(s string) string {
	message := fmt.Sprintf("Dot Matrix Legacy Printer %s", s)
	log.Println(message)
	return message
}

// LaserPrinter - this is the interface that the laser printer will implement.
type LaserPrinter interface {
	PrintMessage() string
}

// Adapter - this is the struct that implements the LaserPrinter interface.
type LaserAdapter struct {
	legacy  LegacyPrinter
	message string
}

// PrintMessage - this is the method that implements the LaserPrinter interface and prints a message
// if the legacy printer is not initialed then it will print the message passed in.
func (p *LaserAdapter) PrintMessage() string {
	if p.legacy != nil {
		return p.legacy.Print(fmt.Sprintf("Adapter: %s", p.message))
	}

	return p.message
}

// Basic Pattern:
// create an interface that represents the new API
// Then create a struct that implements the new API and wraps the old API
// Then create the ...methods for the adapter struct that implements the new API
// Then call the method on the adapter struct to use the new API enabling the old API to be used with the new API
