import logging

# LegacyPrinter is the interface that the legacy printer will implement.
class LegacyPrinter:
    def print(self, s: str) -> str:
        pass

# DotMatrix is the class that implements the LegacyPrinter interface.
class DotMatrix(LegacyPrinter):
    def print(self, s: str) -> str:
        message = "Dot Matrix - %s" % s
        logging.info(message)
        return message

# LaserPrinter is the interface that the laser printer will implement.
class LaserPrinter:
    def print_message(self) -> str:
        pass

# LaserAdapter is the class that implements the LaserPrinter interface.
class LaserAdapter(LaserPrinter):
    def __init__(self, legacy: LegacyPrinter, message: str):
        self._legacy = legacy
        self._message = message

    def print_message(self) -> str:
        if self._legacy is not None:
            return self._legacy.print("Adapter: %s" % self._message)
        return self._message

# create a new instance of the legacy printer.
legacy_printer = DotMatrix()

# create a new instance of the adapter and pass in the legacy printer.
adapter = LaserAdapter(legacy_printer, "Legacy Test Print")

# print the message using the adapter or modern printer.
print(adapter.print_message())

# create a new instance of the adapter and pass in the legacy printer.
adapter = LaserAdapter(None, "Laser Printer Test Print")

# print the message using the adapter or modern printer.
print(adapter.print_message())